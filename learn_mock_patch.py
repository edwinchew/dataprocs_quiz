import unittest
from unittest.mock import patch
import logging

logger = logging.getLogger(__name__)
loghandler = logging.StreamHandler()
logformat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
loghandler.setFormatter(logging.Formatter(logformat))
logger.addHandler(loghandler)
logger.setLevel(logging.DEBUG)

def Recursion(ingress):
    if ingress > 0:
        return ingress + Recursion(ingress - 1)
    else:
        return 0

def Wrapper(ingress):
    return 'wrapper:' + Inner(ingress, None)

def Inner(ingress, dummy):
    return 'inner:' + str(ingress)


class testcase_Recursion(unittest.TestCase):
    """ Test out recursive function with MOCK!
    """

    def setUp(self):
        self.ingress = 3
        self.copy_Recursion = Recursion

    def test_full_recursion(self):
        self.assertEqual(Recursion(self.ingress), 6)

    def test_wrapper(self):
        self.assertEqual(Wrapper(self.ingress), 'wrapper:inner:3')

    @patch('__main__.Inner')
    def test_mock(self, mocked_Inner):
        mocked_Inner.return_value = 'Mocked'
        self.assertEqual(Wrapper(self.ingress), 'wrapper:Mocked')
        self.assertTrue(mocked_Inner.called)
        mocked_Inner.assert_called_with(3, None)

    @patch('__main__.Recursion')
    def test_mock_recursion(self, mocked_Recursion):
        mocked_Recursion.return_value = 100
        self.assertEqual(self.copy_Recursion(self.ingress), 103)
        mocked_Recursion.assert_called_with(2)

if __name__ == '__main__':
    unittest.main()
