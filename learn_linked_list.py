import logging
import unittest
import string, re, copy
from unittest.mock import patch

logging.basicConfig(level=logging.INFO)

class Node(object):
    def __init__(self, data):
        self.data = data
        self.next = None

    def __rshift__(self, next_node):
        # Is this necessary? Nah, but its cooler
        if isinstance(next_node, self.__class__):
            self.next = next_node
            return self.next
        return self

def getStartOfLL(node, dict_of_nodes={}):
    # Really, id is not necessary because node is already a reference
    if id(node.next) not in dict_of_nodes:
        dict_of_nodes[id(node)] = node
        return getStartOfLL(node.next, dict_of_nodes) # Impure!!
    else:
        return node.next.data

class testcase_Node(unittest.TestCase):
    def setUp(self):
        # Why so complicated? For fun yo!
        nodes = [Node(y) for x, y in zip(range(5), iter(string.ascii_lowercase))]
        for index, node in enumerate(nodes):
            self.__dict__[node.data] = node
            index and (nodes[index - 1] >> nodes[index])
        logging.debug([x for x in sorted(self.__dict__.keys()) if not re.match('_', x)])
        # Make a copy of the recursive function in case of Mock
        self.copy_getStartOfLL = getStartOfLL

    def test_LinkedList(self):
        self.assertEqual(self.a.next, self.b)
        self.assertEqual(self.b.next, self.c)
        self.assertEqual(self.c.next, self.d)
        self.assertEqual(self.d.next, self.e)
        self.assertEqual(self.e.next, None)
        copy_b = copy.deepcopy(self.b)
        self.assertNotEqual(id(self.a.next), id(copy_b))
        self.assertEqual(id(self.a.next), id(self.b))
        self.assertNotEqual(self.a.next, copy_b) # Proves that id is not required

    def test_getStartOfLL_patch(self):
        # Could have used the decorator instead but i prefer the look of with
        with patch('__main__.getStartOfLL') as mocked_getStartOfLL:
            mocked_getStartOfLL.return_value = 'Mocked!'
            # Somehow the second argument must be provided because dict is not cleared
            self.assertEqual(self.copy_getStartOfLL(self.a, {}), 'Mocked!')
            mocked_getStartOfLL.assert_called_with(self.b, {id(self.a): self.a})

    def test_getStartOfLL_full(self):
        # Make a loop
        self.e >> self.c
        # Now find the start of loop
        self.assertEqual(getStartOfLL(self.a), self.c.data)

if __name__ == '__main__':
    unittest.main()
