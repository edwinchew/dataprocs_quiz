import logging
import unittest

logging.basicConfig(level=logging.INFO)

def int_to_bin(integer):
    bit = 0
    binary = ['0'] * 4
    while integer > 0:
        if bit >= len(binary):
            binary += ['0'] * 4
        binary[bit] = str(integer & 1)
        integer = integer >> 1
        bit += 1
    return ''.join(binary[::-1]) # Flip the bits

class testcase_Int2Bin(unittest.TestCase):
    def test_1(self):
        self.assertEqual(int_to_bin(0), '0000')
        self.assertEqual(int_to_bin(1), '0001')
        self.assertEqual(int_to_bin(2), '0010')
        self.assertEqual(int_to_bin(3), '0011')

    def test_2(self):
        [self.assertEqual(int_to_bin(x), '{:04b}'.format(x), 'Testing integer {}'.format(x)) for x in range(16)]

    def test_3(self):
        [self.assertEqual(int_to_bin(x), '{:08b}'.format(x), 'Testing integer {}'.format(x)) for x in range(16, 256)]

if __name__ == '__main__':
    unittest.main()
