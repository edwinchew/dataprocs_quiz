import logging
import unittest

logging.basicConfig(level=logging.INFO)

def permutateString(input_string):
    elem_list = list(input_string)
    if len(elem_list) == 1:
        return elem_list
    list_len = len(elem_list)
    this_elem = elem_list.pop(0)
    permutations = []
    for perm in (list(x) for x in permutateString(''.join(elem_list))):
        for index in range(list_len):
            perm_string = ''.join(perm[:index]+[this_elem]+perm[index:])
            if perm_string not in permutations:
                permutations.append(''.join(perm[:index]+[this_elem]+perm[index:]))
    return permutations

class testcase_permutateString(unittest.TestCase):
    def test_permutate_1(self):
        self.assertCountEqual(permutateString('ABC'), ['ABC', 'ACB', 'BAC', 'CAB', 'BCA', 'CBA'])

    def test_permutate_2(self):
        self.assertCountEqual(permutateString('AAB'), ['AAB', 'ABA', 'BAA'])

if __name__ == '__main__':
    unittest.main()
